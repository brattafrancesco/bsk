package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	private Game game;
	
	@Before
	public void setup(){
		game = new Game();
	}
	
	@Test
	public void gameShouldHaveFrameAtPosition0() throws BowlingException{
		game.addFrame(new Frame(1,5));
		assertArrayEquals(new int[]{1,5}, new int[]{game.getFrameAt(0).getFirstThrow(), game.getFrameAt(0).getSecondThrow()});
	}
	
	@Test(expected=BowlingException.class)
	public void gameShouldThrowsExceptionIfIndexIsMoreThan9() throws BowlingException{
		game.getFrameAt(10);
	}
	
	@Test(expected=BowlingException.class)
	public void gameShouldThrowsExceptionIfAddEleventhFrame() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		game.addFrame(new Frame(2,6));
	}
	
	@Test
	public void scoreOfNormalGame() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void scoreOfGameWithSpare() throws BowlingException{
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void scoreOfGameWithStrike() throws BowlingException{
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void scoreOfGameWithStrikeFollowedBySpare() throws BowlingException{
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		addEightFrame();
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void scoreOfGameWithMultipleStrike() throws BowlingException{
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		addEightFrame();
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void scoreOfGameWithMultipleSpare() throws BowlingException{
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		addEightFrame();
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void setFirstBonusThrow() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addSevenFrame();
		game.addFrame(new Frame(8,2));
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void scoreOfGameWithFinalSpare() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addSevenFrame();
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void setSecondBonusThrow() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addSevenFrame();
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void scoreOfGameWithFinalStrike() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addSevenFrame();
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test(expected=BowlingException.class)
	public void gameShouldThrowsExceptionIfNoBonusIsAdded() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addSevenFrame();
		game.addFrame(new Frame(10,0));
		
		game.calculateScore();
	}
	
	@Test(expected=BowlingException.class)
	public void gameShouldThrowsExceptionIfNoSpareBonusIsAllowed() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		game.setFirstBonusThrow(5);
	}
	
	@Test(expected=BowlingException.class)
	public void gameShouldThrowsExceptionIfSecondBonusIsNotAllowed() throws BowlingException{
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		addEightFrame();
		game.setSecondBonusThrow(5);
	}
	
	@Test
	public void scoreOfPerfectGameShouldBe300() throws BowlingException{
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}

	private void addEightFrame() throws BowlingException {
		addSevenFrame();
		game.addFrame(new Frame(2,6));
	}

	private void addSevenFrame() throws BowlingException {
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
	}
}

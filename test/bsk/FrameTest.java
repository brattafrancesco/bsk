package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {
	
	@Test
	public void firstThrowShouldBe2() throws BowlingException{
		Frame frame = new Frame(2,0);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void secondThrowShouldBe4() throws BowlingException{
		Frame frame = new Frame(0,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test(expected=BowlingException.class)
	public void frameWithMoreThan10PinsShouldThrowsException() throws BowlingException{
		new Frame(7,5);
	}
	
	@Test(expected=BowlingException.class)
	public void frameWithNegativePinsShouldThrowsException() throws BowlingException{
		new Frame(-7,0);
	}
	
	@Test
	public void getScoreShouldReturn8() throws BowlingException{
		Frame frame = new Frame(2,6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void bonusShouldBe5() throws BowlingException{
		Frame frame = new Frame(2,6);
		frame.setBonus(5);
		assertEquals(5, frame.getBonus());
	}
	
	@Test
	public void frameShouldBeSpare() throws BowlingException{
		Frame frame = new Frame(1,9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void getScoreOfSpareFrame() throws BowlingException{
		Frame frame = new Frame(1,9);
		frame.setBonus(5);
		assertEquals(15, frame.getScore());
	}
	
	@Test
	public void frameShouldBeStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void getScoreOfStrikeFrame() throws BowlingException{
		Frame frame = new Frame(10,0);
		frame.setBonus(9);
		assertEquals(19, frame.getScore());
	}

}

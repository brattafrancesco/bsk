package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	private static final int MAX_GAME_SIZE = 10;
	private static final int MIN_GAME_SIZE = 0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>();
		firstBonusThrow = 0;
		secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() >= MAX_GAME_SIZE) {
			throw new BowlingException();
		}
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(!(index > MAX_GAME_SIZE -1 || index < MIN_GAME_SIZE)) {
			if(frames.isEmpty()) {
				throw new BowlingException();
			}else {
				return frames.get(index);
			}	
		}else {
			throw new BowlingException();
		}
			
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(frames.get(MAX_GAME_SIZE - 1).isSpare() || frames.get(MAX_GAME_SIZE - 1).isStrike()) {
			this.firstBonusThrow = firstBonusThrow;	
		}else {
			throw new BowlingException();
		}
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(frames.get(MAX_GAME_SIZE - 1).isStrike()) {
			this.secondBonusThrow = secondBonusThrow;
		}else {
			throw new BowlingException();
		}
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i = 0; i < MAX_GAME_SIZE; i++) {
			Frame frame = frames.get(i);
			if(frame.isSpare()) {
				setBonusIfSpare(i, frame);
			}else if(frame.isStrike()) {
				setBonusIfStrike(i, frame);
			}
			score = score + frame.getScore();
		}
		return score;
	}

	private void setBonusIfStrike(int i, Frame frame) throws BowlingException {
		if(frames.indexOf(frame) == MAX_GAME_SIZE - 1) {
			setBonusForLastStrike(frame);
		}else {
			setBonusForIntermediate(i, frame);	
		}
	}

	private void setBonusForIntermediate(int i, Frame frame) throws BowlingException {
		if(frames.get(i+1).isStrike()) {
			if(frames.indexOf(frame) == MAX_GAME_SIZE - 2) {
				if(this.getFirstBonusThrow() != 0) {
					frame.setBonus(frames.get(MAX_GAME_SIZE - 1).getFirstThrow() + this.getFirstBonusThrow());
				}else {
					throw new BowlingException();
				}
			}else {
				frame.setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow());
			}
		}else {
			frame.setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
		}
	}

	private void setBonusForLastStrike(Frame frame) throws BowlingException {
		if(this.getFirstBonusThrow() != 0 || this.getSecondBonusThrow() != 0) {
			frame.setBonus(this.getFirstBonusThrow() + this.getSecondBonusThrow());
		}else {
			throw new BowlingException();
		}
	}

	private void setBonusIfSpare(int i, Frame frame) throws BowlingException {
		if(frames.indexOf(frame) == MAX_GAME_SIZE - 1) {
			setBonusForLastSpare(frame);
		}else {
			frame.setBonus(frames.get(i+1).getFirstThrow());
		}
	}

	private void setBonusForLastSpare(Frame frame) throws BowlingException {
		if(this.getFirstBonusThrow() != 0) {
			frame.setBonus(this.getFirstBonusThrow());
		}else {
			throw new BowlingException();
		}
	}

}
